//
// Created by stiven on 17-10-21.
//

#ifndef POUTINE_SOCKET_H
#define POUTINE_SOCKET_H

#include <SDL2/SDL_net.h>

class Socket{
protected:
    IPaddress ip;
    TCPsocket tcpsock;
    static constexpr unsigned mtu = 1000;
    using type = int;
public:

    Socket(const char *ipAdress);

    Socket();
    int receive(char *data);
    void send(char *data, type size);

    virtual void connect();
    virtual ~Socket();

};



#endif //POUTINE_SOCKET_H
