//

#include <iostream>
#include <SDL2/SDL_net.h>
#include "socket.hpp"

//
// Created by stiven on 18-05-27.
template<class... Ts>
void Socket::log(Ts &&... arg) {
	if (debug) {
		(std::cout << ... << arg) << std::endl;
	}
}

Socket::operator bool() {
	return initialized;
}

Socket::Socket(Uint16 remotePort, Uint16 localPort) {
	log("connecting to port", remotePort);
	log("Local port : ", localPort, "\n\n");

	if (!openPort(localPort))
		return;

	if (!setPort(remotePort))
		return;

	createPacket();

	SDLNet_UDP_Unbind(udPsocket, 0);

	initialized = true;
}

Socket::Socket(std::string const &ip, Uint16 remotePort, Uint16 localPort) {
	log("Connecting to \n\tIP : ", ip, "\n\tPort : ", remotePort);
	log("Local port : ", localPort, "\n\n");


	if (!openPort(localPort))
		return;

	if (!setIpAndPort(ip, remotePort))
		return;

	createPacket();

	/* bind server address to channel 0 */
	if (SDLNet_UDP_Bind(udPsocket, 0, &serverIp) == -1) {
		printf("SDLNet_UDP_Bind: %s\n", SDLNet_GetError());
		return;
	}
	initialized = true;
}

Socket Socket::makeServer(int32_t remotePort, int32_t localPort) {
	return Socket(remotePort, localPort);
}

Socket Socket::makeClient(std::string const &ip, int32_t remotePort, int32_t localPort) {
	return Socket(ip, remotePort, localPort);
}

Socket::~Socket() {
	SDLNet_UDP_Close(udPsocket);
}

bool Socket::openPort(Uint16 port) {

	log("Opening port ", port, "...\n");

	// Sets our sovket with our local port
	udPsocket = SDLNet_UDP_Open(port);

	if (!udPsocket) {
		log("\tSDLNet_UDP_Open failed : ", SDLNet_GetError());
		return false;
	}

	log("\tSuccess!\n\n");
	return true;
}

bool Socket::setPort(Uint16 port) {

	log("Setting up port ( ", port, " )\n");

	// Set IP and port number with correct endianess

	if (SDLNet_ResolveHost(&serverIp, nullptr, port) == -1) {
		log("\tSDLNet_ResolveHost failed : ", SDLNet_GetError());
		return false;
	}

	log("\tSuccess!\n\n");
	return true;
}

bool Socket::setIpAndPort(std::string const &ip, Uint16 port) {

	log("Setting IP ( ", ip, " ) ", "and port ( ", port, " )\n");

	// Set IP and port number with correct endianess
	if (SDLNet_ResolveHost(&serverIp, ip.c_str(), port) == -1) {
		log("\tSDLNet_ResolveHost failed : ", SDLNet_GetError());
		return false;
	}

	log("\tSuccess!\n\n");
	return true;
}

void Socket::createPacket() {

	packet.channel = 0;
	packet.maxlen = mtu;
	// Set the destination host and port
	// We got these from calling SetIPAndPort()
	packet.address.host = serverIp.host;
	packet.address.port = serverIp.port;

}

void Socket::updatePacket(const void *data, int32_t size) {
	packet.data = (Uint8 *) data;
	packet.len = size;

}

//TODO : add working timeout
bool Socket::sendPacket(char const *data, int size) {
	updatePacket(data, size);
	if (SDLNet_UDP_Send(udPsocket, -1, &packet) == 0) {
		log("\tSDLNet_UDP_Send failed : ", SDLNet_GetError(), "\n",
		    "==========================================================================================================\n");
		return false;
	}
	log("sent to: ", packet.address.host, "\n");
	log("length is: ", packet.len, "\n");

	packet.data = (Uint8 *) &returnFlag;
	int begin = SDL_GetTicks();
	int end;
	do {
		SDLNet_UDP_Recv(udPsocket, &packet);
		end = SDL_GetTicks() - begin;
	} while (returnFlag != flag::OK);
	returnFlag = flag::NONE;

	log("receive");
}

bool Socket::receivePacket(void *data) {
	packet.data = (Uint8 *) data;
	bool code = (bool) SDLNet_UDP_Recv(udPsocket, &packet);
	if (code) {
		flag a = flag::OK;
		updatePacket(&a, sizeof(flag));
		SDLNet_UDP_Send(udPsocket, -1, &packet);
	}
	return code;

}

bool Socket::send(const char *data, int size) {
	sendPacket((char const *) &size, sizeof(size));
	while (size > 1) {
		sendPacket(data, mtu < size ? mtu : size);
		data += mtu;
		size -= mtu;
	}


}

int Socket::receive(char *data) {
	int size ;
	while (!receivePacket(&size));

	std::cout << size << std::endl;
	while (size > 1){
		while (!receivePacket(data));
		data += mtu;
		size -= mtu;
	}

	return size;
}
