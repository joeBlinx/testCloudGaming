//
// Created by stiven on 18-05-27.
//

#ifndef UDPTEST_SOCKET_HPP
#define UDPTEST_SOCKET_HPP


#include <cstdint>
#include <string>
#include <SDL2/SDL_net.h>
class Socket {
	enum class flag{
		NONE, OK
	};
#if NDEBUG
	static bool constexpr debug = false;
#else
	static bool constexpr debug = false;
#endif
	static int constexpr mtu = 10000*6;
	static int constexpr timeout = 1000;


	UDPsocket udPsocket;
	IPaddress serverIp;
	UDPpacket packet;
	bool initialized = false;

	flag returnFlag = flag::NONE;

	Socket();
	Socket(Uint16 remotePort, Uint16 localPort);
	Socket(std::string const &ip, Uint16 remotePort, Uint16 localPort);

	Socket(Socket const &) = delete;
	Socket &operator=(Socket const &) = delete;
	Socket(Socket&&) = delete;
	Socket& operator=(Socket&&) = delete;


	bool openPort(Uint16 port);
	bool setPort(Uint16 port);
	bool setIpAndPort(std::string const & ip, Uint16 port);
	void createPacket();
	void updatePacket(const void *data, int32_t size);
	bool sendPacket(char const *data, int size);
	bool receivePacket(void *data);
	template<class ...Ts>
	void log(Ts && ...arg);


public:


	static Socket makeServer(int32_t remotePort, int32_t localPort);
	static Socket makeClient(std::string const &ip, int32_t remotePort, int32_t localPort);

	bool send(const char *data, int32_t size);
	int receive(char *data);
	operator bool();
	~Socket();


};


#endif //UDPTEST_SOCKET_HPP
