//
// Created by stiven on 17-10-21.
//

#include "socket.h"
#include <glm/vec2.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

Socket::Socket(const char *ipAdress) {

	if (SDLNet_ResolveHost(&ip, ipAdress, 5000) == -1) {
		printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		exit(1);
	}

	tcpsock = SDLNet_TCP_Open(&ip);
	if (!tcpsock) {
		tcpsock = SDLNet_TCP_Open(&ip);
		printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		exit(2);
	}

}

Socket::~Socket() {
	SDLNet_TCP_Close(tcpsock);
}

Socket::Socket() {}

void Socket::connect() {

}

void Socket::receive(char *data) {
	type size = 0;

	SDLNet_TCP_Recv(tcpsock, &size, sizeof(type));
	std::cout <<"recv " << size <<std::endl;
	SDLNet_TCP_Send(tcpsock, &size, sizeof(type));
	while(size > 0){
		SDLNet_TCP_Recv(tcpsock, data, mtu < size ? mtu :size);
		SDLNet_TCP_Send(tcpsock, &size, sizeof(type));
		data += mtu;
		size -= mtu;

	}
	SDLNet_TCP_Recv(tcpsock, &size, sizeof(type));

}

void Socket::send(char *data, type size) {

	type res;
	SDLNet_TCP_Send(tcpsock, &size, sizeof(type));
	std::cout << "send " << size <<std::endl;

	SDLNet_TCP_Recv(tcpsock, &res, sizeof(type));
	std::cout << "res " << res << std::endl;
	while (size > 0) {
		SDLNet_TCP_Send(tcpsock, data, mtu < size ? mtu :size);
		SDLNet_TCP_Recv(tcpsock, &res, sizeof(type));
		size -= mtu;
		data += mtu;
	}
	SDLNet_TCP_Send(tcpsock, &size, sizeof(type));

}






