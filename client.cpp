//
// Created by stiven on 18-05-26.
//

#include <zconf.h>
#include <glm/vec2.hpp>
#include <vector>
#include <glish/Vao.hpp>
#include <glish/utils/uniContainer.hpp>
#include "windowSettings.hpp"
#include "window.hpp"
#include "Texture.hpp"
#include "socket/udp/socket.hpp"
#include "compression/jpgd.h"

void client(char const *ip) {
	int32_t remotePort = 3333;
	int32_t localPort = 2222;
	Socket socket = Socket::makeClient(std::string(ip), remotePort, localPort);
	int constexpr width = 200;
	int constexpr height = 100;
	char *data = (char*) malloc( width * height * 3);


	windowSettings settings{"cloud Gaming", width, height};
	Window win{settings};
	std::vector<glm::vec2> vertices{
			{-1, 1},
			{-1, -1},
			{1,  1},
			{1,  -1}
	};
	std::vector<glm::vec2> uvs{
			{0, 1},
			{0, 0},
			{1, 1},
			{1, 0}
	};
	glish::Vao<2> vao;
	vao.addVbo(0, vertices);
	vao.addVbo(1, uvs);
	glish::UniContainer program{glish::shaderFile{GL_VERTEX_SHADER, "../vertexClient.glsl"},
	                            glish::shaderFile{GL_FRAGMENT_SHADER, "../fragmentClient.glsl"}};
	program.add<int>("image");
	program.use();
	bool run = true;
	program.update("image", 0);
	glish::getError();
	SDL_Event ev;
	Texture texture;
	int a;
	Uint8 const * key = SDL_GetKeyboardState(&a);
	int size;
	int widthImage = width;
	int heightImage = height;
	int actualcomps = 3;
	while (run) {

		glClear(GL_COLOR_BUFFER_BIT);

		while (SDL_PollEvent(&ev)) {
			switch (ev.type) {
				case SDL_QUIT:
					run = false;
					break;
				case SDL_KEYDOWN:
					if (ev.key.keysym.sym == SDLK_ESCAPE) {
						run = false;
					}
					break;
			}
		}
		socket.send((char*)key, a);
		size = socket.receive(data);
		unsigned char  * image = jpgd::decompress_jpeg_image_from_memory((unsigned char *)data, size, &widthImage, &heightImage, &actualcomps, 3);
		texture.update(image, width, height);
//		texture.update(data, width, height);
		//write(data, 3 * 1366 * 768);

		texture.bindTexture(0);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glish::getError();
		SDL_GL_SwapWindow(win);
	}
	free(data);
}