//
// Created by stiven on 14/12/15.
//


#include <SDL2/SDL_image.h>
#include "Texture.hpp"
#include <iostream>
#include <string>
#include <stdexcept>
#include <array>
#include <glish/utils/log.hpp>
using namespace glish;
GLuint Texture::get() const {
	return text;
}

void Texture::bindTexture(int number) const {
	if (text != 0) {
		glActiveTexture(GL_TEXTURE0 + number);
		getError();
		glBindTexture(target, text);
		getError();
	} else {
		std::cerr << "Texture is not initialized" << std::endl;
	}

}

Texture::Texture(void *data, int width, int height, GLenum target) : target(target) {

	glGenTextures(1, &text);
	getError();
	glBindTexture(target, text);
	getError();

	loadTexture(data, target, width, height);
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	getError();
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	getError();
	glBindTexture(target, 0);
	getError();


}

Texture::Texture(GLuint texture, GLenum target) : text(texture), target(target) {

}

Texture::Texture(Texture &&texture) : text(texture.text), target(texture.target) {
	texture.text = 0;
}

Texture::Texture() {

	glGenTextures(1, &text);
	getError();
	glBindTexture(target, text);
	getError();

	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	getError();
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	getError();
	glBindTexture(target, 0);
	getError();
}

Texture::Texture(GLenum target) : target(target) {
	glGenTextures(1, &text);
	getError();
	glBindTexture(target, text);
	getError();
}

void Texture::loadTexture(void *data, GLenum target, int width, int height) {


	glTexImage2D(target, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	getError();

}
void Texture::update(void *data, int width, int height) {

	bindTexture(0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	getError();

}

Texture &Texture::operator=(Texture &&texture) {
	text = texture.text;
	target = texture.target;
	texture.text = 0;
	return *this;
}

Texture::~Texture() {
	if (text != 0 && !hasBeenCopied) {
		glDeleteTextures(1, &text);
		getError();
	}
}


Texture::Texture(const Texture &oldTexture) : text(oldTexture.text), target(oldTexture.target) {
	oldTexture.hasBeenCopied = true;
}

Texture &Texture::operator=(Texture const &oldTexture) {
	text = oldTexture.text;
	target = oldTexture.target;
	oldTexture.hasBeenCopied = true;
	return *this;
}

