//
// Created by stiven on 18-05-26.
//

#include <cstdlib>
#include <SDL2/SDL_timer.h>
#include <glish/utils/log.hpp>
#include <GL/glew.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_events.h>
#include <glish/program/Program.hpp>
#include <glish/utils/uniContainer.hpp>
#include <glish/Vao.hpp>
#include "window.hpp"
#include "windowSettings.hpp"
#include "socket/udp/socket.hpp"
#include "compression/jpge.h"

void server() {
	int32_t remotePort = 2222;
	int32_t localPort = 3333;
	Socket server = Socket::makeServer(remotePort, localPort);
	int constexpr width = 200;
	int constexpr height = 100;

	windowSettings settings{"cloud Gaming", width, height};
	Window win{settings};
	std::vector<glm::vec2> vertices{
			{0, 1},
			{0, 0},
			{1, 1},
			{1, 0}
	};
	//SDL_HideWindow(win);
	glish::Vao<1> vao;
	vao.addVbo(0, vertices);
	glish::UniContainer program{glish::shaderFile{GL_VERTEX_SHADER, "../vertex.glsl"},
	                            glish::shaderFile{GL_FRAGMENT_SHADER, "../fragment.glsl"}};
	program.add<float>("x");
	program.use();


	int size = width * height * 3;
	char *data = (char*) malloc(size);
	bool run = true;
	char * image = (char*) malloc(size);
	
	glish::getError();
	SDL_Event ev;
	Uint8 key[512]={};
	float x = 0.01;
	while (run) {
		glClear(GL_COLOR_BUFFER_BIT);

		server.receive((char*)key);
		if(key[SDL_SCANCODE_S] ){
			x+=0.01;
		}else if(key[SDL_SCANCODE_A]){
			x-=0.01;
		}

		program.update("x", x);

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glish::getError();

		SDL_GL_SwapWindow(win);
		glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
		glish::getError();
		jpge::params param;
		param.m_two_pass_flag = true;

		if(!jpge::compress_image_to_jpeg_file_in_memory(image, size,
		                                                width, height, 3, (jpge::uint8*)data, param)){
			std::cerr << "problem" << std::endl;
		}
//		server.send(data, 3 * width * height );
		server.send(image, size);
		size = width*height*3;
		SDL_Delay(17);

	}
	free(data);
}