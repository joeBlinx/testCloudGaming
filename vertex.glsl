#version 400



layout(location = 0) in vec2 pos;

uniform float x;
void main() {
    gl_Position = vec4(pos.x+x, pos.y, 0, 1);
}
