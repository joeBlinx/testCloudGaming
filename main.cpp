#include <iostream>
#include <GL/glew.h>
#include "windowSettings.hpp"
#include "window.hpp"
#include "socket/server.h"
#include "Texture.hpp"
#include <glish/Vao.hpp>
#include <glish/utils/uniContainer.hpp>
#include <fstream>


void write(void *data, int size) {
	static int a = 0;
	std::ofstream file{"file" + std::to_string(a++) + ".bmp", std::ios::binary};
	unsigned char bmpfileheader[14] = {'B', 'M', 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0};
	unsigned char bmpinfoheader[40] = {40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 24, 0};
	unsigned char bmppad[3] = {0, 0, 0};
	int filesize = size * 4;
	int w = 1366;
	int h = 768;
	bmpfileheader[2] = (unsigned char) (filesize);
	bmpfileheader[3] = (unsigned char) (filesize >> 8);
	bmpfileheader[4] = (unsigned char) (filesize >> 16);
	bmpfileheader[5] = (unsigned char) (filesize >> 24);

	bmpinfoheader[4] = (unsigned char) (w);
	bmpinfoheader[5] = (unsigned char) (w >> 8);
	bmpinfoheader[6] = (unsigned char) (w >> 16);
	bmpinfoheader[7] = (unsigned char) (w >> 24);
	bmpinfoheader[8] = (unsigned char) (h);
	bmpinfoheader[9] = (unsigned char) (h >> 8);
	bmpinfoheader[10] = (unsigned char) (h >> 16);
	bmpinfoheader[11] = (unsigned char) (h >> 24);
	file.write((char *) bmpfileheader, 14);
	file.write((char *) bmpinfoheader, 40);
	file.write((char *) bmppad, 3);

	file.write((char *) data, size * 4);

}
void server();

void client(char const *ip);


int main(int argc, char **argv) {
	SDLNet_Init();
	if (argc > 1) {
		client(argv[1]);
	} else {

		server();
	}
	SDLNet_Quit();
	return 0;
}
